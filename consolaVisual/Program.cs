﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bibliotecaVisual;

namespace consolaVisual
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            Boolean menu = true;

            while (menu == true)
            {
                Console.WriteLine("\t\n*** MENÚ ***");
                Console.WriteLine("1. Calcular si dos números son divisibles.");
                Console.WriteLine("2. Calcular la multiplicación entre dos valores.");
                Console.WriteLine("3. Calcular media.");
                Console.WriteLine("4. Mostrar los pares entre dos números.");
                Console.WriteLine("5. Salir.");
                Console.WriteLine("Opción: ");
                opcion = int.Parse(Console.ReadLine());
                while (opcion < 1 || opcion > 5)
                {
                    Console.WriteLine("Error. Introduce un valor entre 1 y 5.");
                    Console.WriteLine("Opción: ");
                    opcion = int.Parse(Console.ReadLine());
                }

                if (opcion == 1)
                {
                    Console.WriteLine("Escribe los números que quieres comprobar sin son divisbles: ");
                    Console.WriteLine("Num1: ");
                    int num1 = int.Parse(Console.ReadLine());
                    Console.WriteLine("Num2: ");
                    int num2 = int.Parse(Console.ReadLine());
                    Boolean divisible = bibliotecaVisual.Class1.comprobarSonDivisibles(num1, num2);

                    if (divisible == true)
                    {
                        Console.WriteLine("Son divisibles");
                    }
                    else Console.WriteLine("No son divisibles");
                }
                else if (opcion == 2)
                {
                    Console.WriteLine("Escribe los números que quieres multiplicar: ");
                    Console.WriteLine("Num1: ");
                    int num1 = int.Parse(Console.ReadLine());
                    Console.WriteLine("Num2: ");
                    int num2 = int.Parse(Console.ReadLine());

                    int  multiplicacion = bibliotecaVisual.Class1.multiplicar(num1, num2);
                    Console.WriteLine("La multiplicación entre " + num1 + " y " + num2 + " es " + multiplicacion);

                }
                else if (opcion == 3)
                {
                    Console.WriteLine("Escribe la cantidad de números que vas a introducir: ");
                    int dimension = int.Parse(Console.ReadLine());
                    int[] v = new int[dimension];

                    float media = bibliotecaVisual.Class1.media(v);

                    Console.WriteLine("La media es " + media);
                   
                }
                else if (opcion == 4)
                {
                    Console.WriteLine("Escribe los dos números: ");
                    Console.WriteLine("Num1: ");
                    int num1 = int.Parse(Console.ReadLine());
                    Console.WriteLine("Num2: ");
                    int num2 = int.Parse(Console.ReadLine());
                    bibliotecaVisual.Class1.paresEntreDosNumeros(num1, num2);
                }
                else if (opcion == 5)
                {
                    Console.WriteLine("***FIN***");
                    menu = false;
                }

            }
        }
    }
}
